from flask import Blueprint, Response, jsonify, stream_with_context
import ckan.plugins.toolkit as toolkit
import os
import requests
from urllib.parse import urlparse, urlunparse

# Define Blueprint
uri = Blueprint('uri', __name__)

@uri.route('/custom/<id>', methods=['GET'])
def dataset_json(id):
    try:
        # Fetch dataset by id
        context = {'user': toolkit.c.user or toolkit.c.author}
        dataset = toolkit.get_action('package_show')(context, {'id': id})
        
        # Return dataset as JSON
        return jsonify(dataset)

    except toolkit.ObjectNotFound:
        return 'Dataset not found', 404
    except toolkit.NotAuthorized:
        return 'Not authorized to see this dataset', 403
    
# New endpoint for fetching resources
@uri.route('/custom/resources/<dataset_id>/<int:index>', methods=['GET'])
def get_resource(dataset_id, index):
    try:
        print("get_resource called: ", dataset_id, " ", index)
        
        # Fetch dataset by id
        context = {'user': toolkit.c.user or toolkit.c.author}
        dataset = toolkit.get_action('package_show')(context, {'id': dataset_id})
        
        # Get the resource by index
        if index < len(dataset.get('resources', [])):
            resource = dataset['resources'][index]
            resource_url = resource['url']
            print("resource_url: ", resource_url)

            # LOCAL FIX
            # Parse the URL into components
            url_components = urlparse(resource_url)

            # Add the port 8080 to the netloc component
            netloc_with_port = "{}:8080".format(url_components.hostname)

            # Rebuild the URL with the port
            new_url = urlunparse((
                url_components.scheme,
                netloc_with_port,
                url_components.path,
                url_components.params,
                url_components.query,
                url_components.fragment
            ))
            
            # Fetch the content of the resource URL
            req = requests.get(new_url, stream=True)

            # Stream the content of the resource
            def generate():
                for chunk in req.iter_content(chunk_size=1024):
                    yield chunk
            return Response(stream_with_context(generate()), content_type=req.headers['content-type'])

        else:
            return 'Resource index out of range', 404

    except toolkit.ObjectNotFound:
        return 'Dataset not found', 404
    except toolkit.NotAuthorized:
        return 'Not authorized to see this dataset', 403